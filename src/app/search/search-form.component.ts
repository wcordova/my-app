import { Component, EventEmitter, Output } from "@angular/core";

@Component({
    selector: "SearchForm",
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row">
    <TextField [(ngModel)]="textFieldValue" hint="Ingresar texto..."
    required></TextField>
    <Label text="*"></Label>
    </FlexboxLayout>
    <Button text="Buscar!" (tap)="onButtonTap()"></Button>
    `
})
export class SearchFormComponent {
    textFieldValue: string = "";
    @Output() search: EventEmitter<string> = new EventEmitter();

    onButtonTap(): void {
        console.log(this.textFieldValue);
        if (this.textFieldValue.length > 2) {
            this.search.emit(this.textFieldValue);
        }
    }
}